# Jenkins_CI Cookbook

Installs and configures Jenkins CI, Java 8 from Oracle, Maven, Git and apt.

It doesn't install any Jenkins plugins.

It maps port 8080 to port 10080 of the host and it gets IP 192.168.33.11.

It will build an Ubuntu-16.04 box and it will test using InSpec.
