#
# Cookbook Name:: vaadin_ci
# Recipe:: default
#
# Copyright (c) 2016 The Authors, All Rights Reserved.

include_recipe 'apt'
include_recipe 'java'
include_recipe 'git'
include_recipe 'maven'
include_recipe 'jenkins::master'
